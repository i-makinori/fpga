# Field Programmable Gate Array による現在の計算機科学の根本理解に向けた学習


# 参考文献

### 書籍

- 小林優 著､ FPGAボードで学ぶ組込みシステム開発入門 ～Altera編～ (初版)
-  http://gihyo.jp/book/2011/978-4-7741-4839-7/support

updown : 第二章


### error 98 JTAG -- server error の解決

quote from ...

- https://forums.intel.com/s/question/0D50P00003yyPnBSAU/quartus-ii-jtag-server-error-code-89?language=en_US
- http://fabionotes.blogspot.com/2014/05/altera-de0.html

```
Error (209053): Unexpected error in JTAG server -- error code 89
Error (209012): Operation failed


add the file /etc/udev/rules.d/98-altera.rules with the following content


# USB-Blaster

SUBSYSTEM=="usb", ENV{DEVTYPE}=="usb_device", ATTRS{idVendor}=="09fb", ATTRS{idProduct}=="6001", MODE="0666", SYMLINK+="usbblaster/%k"
SUBSYSTEM=="usb", ENV{DEVTYPE}=="usb_device", ATTRS{idVendor}=="09fb", ATTRS{idProduct}=="6002", MODE="0666", SYMLINK+="usbblaster/%k"
SUBSYSTEM=="usb", ENV{DEVTYPE}=="usb_device", ATTRS{idVendor}=="09fb", ATTRS{idProduct}=="6003", MODE="0666", SYMLINK+="usbblaster/%k"


# USB-Blaster II
SUBSYSTEM=="usb", ENV{DEVTYPE}=="usb_device", ATTRS{idVendor}=="09fb", ATTRS{idProduct}=="6010", MODE="0666", SYMLINK+="usbblaster2/%k"
SUBSYSTEM=="usb", ENV{DEVTYPE}=="usb_device", ATTRS{idVendor}=="09fb", ATTRS{idProduct}=="6810", MODE="0666", SYMLINK+="usbblaster2/%k"
```

quote end.


