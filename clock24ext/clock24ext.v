module CLOCK24EXT(
               input CLK, RST,
               input [2:0] nBUTTON,
					input [3:0] SW, // hour mode
               output [9:0] LEDG, // 9:hour mode, 6:4 : sec digit 2 , 3:0 : sec digit 1
               output [7:0] nHEX0, nHEX1, nHEX2, nHEX3
					);

   wire                     CASEC, CAMIN;
   wire                     MODE, SELECT, ADJUST;
	wire                     SW3_TEMP, SW2_TEMP, SW1_TEMP, HOUR_MODE;
   wire                     SECCLR, MININC, HOURINC;
   wire                     SECON, MINON, HOURON;
   wire                     EN1HZ, SIG2HZ;
   wire [2:0]               btn;
   wire [3:0]               SECL, MINL,HOURL;
   wire [2:0]               SECH, MINH;
   wire [1:0]               HOURH;
	
   
	parameter HOUR12=1'b1, HOUR24 = 1'b0;
	

   CNT1SEC CNT1SEC( .CLK(CLK), .RST(RST), .EN1HZ(EN1HZ), .SIG2HZ(SIG2HZ));

   BTN_IN BTN_IN (.CLK(CLK), .RST(RST),  .nBIN(nBUTTON),
                  .BOUT({MODE, SELECT, ADJUST}));
	
	SW_IN SW_IN (.CLK(CLK), .RST(RST), .SW_HARDWARE(SW),
	             .SW_SOFTWARE({SW3_TEMP, SW2_TEMP, SW1_TEMP, HOUR_MODE}));

   SECCNT SEC(.CLK(CLK), .RST(RST), .EN(EN1HZ), .CLR(SECCLR),
              .QH(SECH), .QL(SECL), .CA(CASEC));

   MINCNT MIN(.CLK(CLK), .RST(RST), .EN(CASEC), .INC(MININC),
              .QH(MINH), .QL(MINL), .CA(CAMIN));

   HOURCNT HOUR(.CLK(CLK), .RST(RST), .EN(CAMIN), .INC(HOURINC), .HOUR_MODE(HOUR_MODE),
                .QH(HOURH), .QL(HOURL));

   STATE STATE( .CLK(CLK), .RST(RST), .SIG2HZ(SIG2HZ),
                .MODE(MODE), .SELECT(SELECT), .ADJUST(ADJUST),
                .SECCLR(SECCLR), .MININC(MININC), .HOURINC(HOURINC),
                .SECON(SECON), .MINON(MINON), .HOURON(HOURON));


   SEG7DEC ML(.SAPLES(0), .DIN(MINL), .EN(MINON), .nHEX(nHEX0[6:0]));
   SEG7DEC MH(.SAPLES(0), .DIN({1'b0, MINH}), .EN(MINON), .nHEX(nHEX1[6:0]));
   SEG7DEC HL(.SAPLES(0), .DIN(HOURL), .EN(HOURON), .nHEX(nHEX2[6:0]));
	SEG7DEC HH(.SAPLES(HOUR_MODE==HOUR24 & HOURH==2'd00), .DIN({2'd00, HOURH}), .EN(HOURON), .nHEX(nHEX3[6:0]));
	
   assign LEDG[3:0] = (SECON==1'b1) ? SECL: 4'h0;
   assign LEDG[6:4] = (SECON==1'b1) ? SECH: 3'h0;
   assign LEDG[8:7] = 2'h0;
	assign LEDG[9]   = HOUR_MODE;

   assign nHEX0[7] = 1'b1;
   assign nHEX1[7] = 1'b1;
   assign nHEX2[7] = 1'b0;
   assign nHEX3[7] = 1'b1;

endmodule
   
   

   
                    
