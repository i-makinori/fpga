module SECCNT(
              input            CLK, RST,
              input            EN, CLR,
              output reg [2:0] QH,
              output reg [3:0] QL,
              output           CA
              );

   always @(posedge CLK, posedge RST) begin
      if( RST )
        QL <= 4'd0;
      else if (CLR==1'b1)
        QL <= 4'd0;
      else if (EN==1'b1) begin
         if (QL==4'd9)
           QL <= 4'd0;
         else
           QL <= QL + 1'b1;
      end
   end // always @ (posedge CLK, posedge RST)

   always @(posedge CLK, posedge RST) begin
      if(RST)
        QH <= 3'd0;
      else if(CLR==1'b1)
        QH <= 3'd0;
      else if(EN==1'b1 && QL==4'd9) begin
         if (QH==3'd5)
           QH <= 3'd0;
         else
           QH <= QH + 1'b1;
      end
   end // always @ (posedge CLK, POSEDGE RST)

   assign CA = (QH==3'd5 && QL==4'd9 && EN==1'b1);

endmodule // SECCNT

