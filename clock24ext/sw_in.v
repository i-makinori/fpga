module SW_IN(
             input        CLK, RST,
             input [0:3]  SW_HARDWARE,
             output reg [0:3] SW_SOFTWARE
             );

   // clock and timing
   reg [20:0]             cnt;
   wire                   en40hz = (cnt==1250000-1);

   always @(posedge CLK, posedge RST) begin
      if (RST)
        cnt <= 21'b0;
      else if (en40hz)
        cnt <= 21'b0;
      else
        cnt <= cnt+ 21'b1;
   end

   // chattering
   reg [3:0] ff1, ff2;

   always @(posedge CLK, posedge RST) begin
      if (RST) begin
         ff2 <= SW_HARDWARE;
         ff1 <= SW_HARDWARE;
      end
      else if (en40hz) begin
         ff2 <= ff1;
         ff1 <= SW_HARDWARE;
      end
   end

   // switch value after remove cattering
   wire [3:0] temp = ff1 & SW_HARDWARE;

   always @(posedge CLK, posedge RST) begin
      SW_SOFTWARE <= temp;
   end

endmodule
