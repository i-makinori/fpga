module UPDOWN
  ( input CLK, RST,
    input [2:0]      nBUTTON,
    output reg [6:0] nHEX0
    );
   
   
   wire              reset, down, up;
   BTN_IN BTN_IN (CLK, RST, nBUTTON, {reset, down, up});

   reg [3:0]         udcnt;
   always @(posedge CLK, posedge RST) begin
      if (RST)
        udcnt <= 4'h0;
      else if (reset)
        udcnt <= 4'h0;
      else if (up)
        udcnt <= udcnt + 4'h1;
      else if (down)
        udcnt <= udcnt - 4'h1;
   end
   
   always @* begin
      case (udcnt)
        4'h0: nHEX0 = 7'b1000000;
        4'h1: nHEX0 = 7'b1111001;
        4'h2: nHEX0 = 7'b0100100;
        4'h3: nHEX0 = 7'b0110000;

        4'h4: nHEX0 = 7'b0011001;
        4'h5: nHEX0 = 7'b0010010;
        4'h6: nHEX0 = 7'b0000010;
        4'h7: nHEX0 = 7'b1011000;

        4'h8: nHEX0 = 7'b0000000;
        4'h9: nHEX0 = 7'b0010000;
        4'ha: nHEX0 = 7'b0001000;
        4'hb: nHEX0 = 7'b0000011;

        4'hc: nHEX0 = 7'b1000110;
        4'hd: nHEX0 = 7'b0100001;
        4'he: nHEX0 = 7'b0000110;
        4'hf: nHEX0 = 7'b0001110;

        default : nHEX0 = 7'bxxxxxxx;
      endcase // case (udcnt)
   end // always @ *

endmodule // UPDOWN

